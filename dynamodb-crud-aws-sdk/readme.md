## Node project to interact with DynamoDb using aws-sdk

Here you can interact with DynamoDB, in this example you can do next operations:

    * Create a table
    * Insert row
    * Update row
    * Read row
    * Read all rows
    * Delete row

- Create a table
    ```
    node -e 'require("./app.js").createTable()'
    ```

- Insert rows
    ```
    node -e 'require("./app.js").addMovie("The Fast and the Furious", "100")'
    node -e 'require("./app.js").addMovie("2 Fast 2 Furious", "36")'
    node -e 'require("./app.js").addMovie("The Fast and the Furious: Tokyo Drift", "38")'
    node -e 'require("./app.js").addMovie("Fast & Furious", "29")'
    node -e 'require("./app.js").addMovie("Fast Five", "77")'
    node -e 'require("./app.js").addMovie("Fast & Furious 6", "70")'
    node -e 'require("./app.js").addMovie("Furious 7", "81")'
    node -e 'require("./app.js").addMovie("The Fate of the Furious", "67")'
    node -e 'require("./app.js").addMovie("Fast & Furious Presents: Hobbs & Shaw", "67")'
    ```

- Update row
    ```
    node -e 'require("./app.js").updateMovieScore("The Fast and the Furious", 53)'

    ```
- Read row
    ```
    node -e 'require("./app.js").getMovie("The Fast and the Furious")'
    ```
- Read all rows
    ```
    node -e 'require("./app.js").getAllMovies()'
    ```
- Delete row
    ```
    node -e 'require("./app.js").deleteMovie("Fast & Furious Presents: Hobbs & Shaw")'
    ```


**Note:** Check your credential.

`cat  ~/.aws/credentials`


working 11/19/2019 consulted on https://www.ryanjyost.com/node-dynamodb-intro-tutorial/