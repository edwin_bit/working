Node project using serverless, express and aws-sdk

**commands for run offline:**
```
npm install --save express serverless-http
npm install --save aws-sdk body-parser
sls offline start
```
**commands for run on cloud:**
```
sls deploy
```

**Export endpoint:**
```
export BASE_DOMAIN=https://bgaok7797f.execute-api.us-east-1.amazonaws.com/dev
or 
export BASE_DOMAIN=http://localhost:3000
```

**Create User**
```
curl -H "Content-Type: application/json" -X POST ${BASE_DOMAIN}/users -d '{"userId": "alexdebrie1", "name": "Alex DeBrie"}'
curl -H "Content-Type: application/json" -X POST http://localhost:3000/users -d '{"userId": "alexdebrie1", "name": "Alex DeBrie"}'
```

**Retrieve the user with the GET /users/:userId**
```
curl -H "Content-Type: application/json" -X GET ${BASE_DOMAIN}/users/alexdebrie1
curl -H "Content-Type: application/json" -X GET http://localhost:3000/users/alexdebrie1
```


**Source:** https://serverless.com/blog/serverless-express-rest-api/

**Note:** Working on 11/19/2019