// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
	baseHref: '/',
	firebase: {
		apiKey: 'AIzaSyBJja4jqtJ203W_6OfwbLmlyHUsTJwlNP8',
		authDomain: 'af2-lists-1567c.firebaseapp.com',
		databaseURL: 'https://af2-lists-1567c.firebaseio.com',
		projectId: 'af2-lists-1567c',
		storageBucket: 'af2-lists-1567c.appspot.com',
		messagingSenderId: '920264506943'
	}
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
