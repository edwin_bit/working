export const environment = {
  production: true,
	baseHref: '/',
	firebase: {
		apiKey: 'AIzaSyBJja4jqtJ203W_6OfwbLmlyHUsTJwlNP8',
		authDomain: 'af2-lists-1567c.firebaseapp.com',
		databaseURL: 'https://af2-lists-1567c.firebaseio.com',
		projectId: 'af2-lists-1567c',
		storageBucket: 'af2-lists-1567c.appspot.com',
		messagingSenderId: '920264506943'
	}
};
