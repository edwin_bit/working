// Libraries
import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule } from '@angular/common';

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { ToastrModule } from 'ngx-toastr';

import { environment } from '../environments/environment';

// Components
import { AppComponent } from './app.component';
import { FirstComponent } from './components/first/first.component';
import { SecondComponent } from './components/second/second.component';
import { LoginComponent } from './components/login/login.component';
import { MenuComponent } from './components/menu/menu.component';
import { UserComponent } from './components/user/user.component';
import { RegisterComponent } from './components/register/register.component';
import { TopNavComponent } from './components/top-nav/top-nav.component';
import { UserResolver } from "./components/user/ user.resolver";
import { InvestsComponent } from './components/index.components';
import { InvestComponent } from './components/index.components';
import { InvestListComponent } from './components/index.components';
import { ModalComponent } from './components/index.components';

// Services
import { AuthService } from "./services/index.services";
import { UserService } from "./services/index.services";
import { AuthGuard } from "./services/index.services";
import { ModalService } from "./services/index.services";
import { InvestService } from "./services/index.services";

// Routing
import { AppRoutingModule } from './app-routing.module'; // should be at the end

@NgModule({
  declarations: [
    AppComponent,
		InvestsComponent,
		InvestComponent,
		InvestListComponent,
    FirstComponent,
    SecondComponent,
    MenuComponent,
    LoginComponent,
    UserComponent,
    RegisterComponent,
		ModalComponent,
		TopNavComponent,
  ],
  imports:[
    BrowserModule,
    FormsModule,
    CommonModule,
    NgtUniversalModule,   
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
		ToastrModule.forRoot(),
  ],
	providers: [
		InvestService,
		ModalService,
		UserResolver,
		AuthService,
		UserService,
		AuthGuard,
	],
})
export class AppModule { }



