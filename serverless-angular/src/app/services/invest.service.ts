import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Invest } from '../models/index.models';
import { Utils } from '../shared/index.shared';

@Injectable({
	providedIn: 'root'
})
export class InvestService {
	public investList: AngularFireList<any>;
	public selectedInvest$: BehaviorSubject<Invest>;

	public selectedInvest = new Invest();

	constructor(private firebase: AngularFireDatabase) {
		this.selectedInvest$ = new BehaviorSubject(this.selectedInvest);
	}

	getData() {
		this.investList = this.firebase.list('invests');
		return this.investList;
	}

	insertInvest(invest: Invest) {
		this.investList.push({
			broker: invest.broker,
			dateTime: Utils.getDateTime(),
			dateTimeBuy: Utils.formatDateTime(invest.dateTimeBuy),
			dateTimeApply: Utils.formatDateTime(invest.dateTimeApply),
			dateTimeSell: Utils.formatDateTime(invest.dateTimeSell),
			priceBuy: invest.priceBuy,
			priceSell: invest.priceSell,
			quantity: invest.quantity,
			total: invest.total,
			commission: invest.commission,
			profit: invest.profit,
			type: invest.type,
			instrument: invest.instrument,
		});
	}

	updateInvest(invest: Invest) {
		this.investList.update(invest.$key,
			{
				broker: invest.broker,
				dateTimeBuy: Utils.formatDateTime(invest.dateTimeBuy),
				dateTimeApply: Utils.formatDateTime(invest.dateTimeApply),
				dateTimeSell: Utils.formatDateTime(invest.dateTimeSell),
				priceBuy: invest.priceBuy,
				priceSell: invest.priceSell,
				quantity: invest.quantity,
				total: invest.total,
				commission: invest.commission,
				profit: invest.profit,
				type: invest.type,
				instrument: invest.instrument,
			});
	}

	deleteInvest($key: string) {
		this.investList.remove($key);
	}

}
