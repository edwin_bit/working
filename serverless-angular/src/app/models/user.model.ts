export class User {
	id: number;
	email: string;
	username: string;
	password: string;
	firstName: string;
	lastName: string;
	image: string;
	provider: string;

	// TODO: constructor for all classes
	// constructor(){
	//   this.image = "";
	//   this.name = "";
	//   this.provider = "";
	// }
}
