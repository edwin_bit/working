import { Utils } from '../shared/index.shared';

export class Invest {
	$key: string;
	dateTime: string;
	dateTimeBuy: string;
	dateTimeApply: string;
	dateTimeSell: string;
	broker: string;
	priceBuy: number;
	priceSell: number;
	quantity: number;
	total: number;
	commission: number;
	profit: number;
	type: string;
	instrument: string;

	constructor() {
		const defaultDate = Utils.formatDateTime(new Date().toDateString());
		this.$key = null;
		this.broker = Utils.BROKERS[0];
		this.dateTime = '';
		this.dateTimeBuy = defaultDate;
		this.dateTimeApply = defaultDate;
		this.dateTimeSell = defaultDate;
		this.priceBuy = 0;
		this.priceSell = 0;
		this.quantity = 0;
		this.total = 0;
		this.commission = 0;
		this.profit = 0;
		this.type = Utils.TYPES[0];
		this.instrument = '';
	}
}
