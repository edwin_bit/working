import * as moment from 'moment';

export class Utils {

	static defaultFormatDate = 'YYYY-MM-DD';

	static BROKERS = [
		'Scotiabank',
		'Bitso'
	];

	static TYPES = [
		'open',
		'closed'
	];

	static BROKERS_APPLY_DATE = [
		'Scotiabank'
	];

	static formatDateTime(date: string, format: string = Utils.defaultFormatDate): string {
		return moment.utc(moment(date, format)).local().format(format);
	}

	static getDateTime(): string {
		return moment.utc(new Date()).local().format(Utils.defaultFormatDate);
	}

	static plusDays(fromDate, quantityPlusDays): string {
		return moment.utc(moment(fromDate, Utils.defaultFormatDate)).add(quantityPlusDays, 'days').local().format(Utils.defaultFormatDate);
	}

	static shouldActivateApplyDate(broker: string): string {
		return this.BROKERS_APPLY_DATE.find(item => item === broker);
	}
}
