import { Component, OnInit } from '@angular/core';
import { InvestService } from '../../../services/index.services';
import { Invest } from '../../../models/index.models';
import { ToastrService } from 'ngx-toastr';
import { ModalService } from '../../../services/index.services';
import { Utils } from '../../../shared/utils';

@Component({
	selector: 'app-invest-list',
	templateUrl: './invest-list.component.html',
	styleUrls: ['./invest-list.component.scss']
})
export class InvestListComponent implements OnInit {
	investList: Invest[];
	constructor(private investService: InvestService, private tostr: ToastrService, private modalService: ModalService) {
		this.investService.selectedInvest$.subscribe(invest => this.investService.selectedInvest = invest);
	}

	ngOnInit() {
		const investList = this.investService.getData();
		investList.snapshotChanges().subscribe(item => {
			this.investList = [];
			item.forEach(element => {
				const invest = element.payload.toJSON();
				invest['$key'] = element.key;
				this.investList.push(invest as Invest);
			});
		});
	}

	onEdit(inv: Invest) {
		this.investService.selectedInvest$.next(Object.assign({}, inv));
		this.modalService.open('invest-modal');
	}

	onDelete(key: string) {
		if (confirm('Are you sure to delete this record ?') === true) {
			this.investService.deleteInvest(key);
			this.tostr.warning('Deleted Successfully', 'Invest register');
		}
	}

	openModal(id: string) {
		this.modalService.open(id);
		this.investService.selectedInvest$.next(new Invest());
	}

}
