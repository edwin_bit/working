import { Component, OnInit } from '@angular/core';
import { InvestService } from '../../services/index.services';
import { AuthService } from '../../services/index.services';

@Component({
	selector: 'app-invests',
	templateUrl: './invests.component.html',
	styleUrls: ['./invests.component.scss']
})
export class InvestsComponent implements OnInit {

	constructor(
		private investService: InvestService,
		// public authService: AuthService
	) { }

	ngOnInit() {}

}
