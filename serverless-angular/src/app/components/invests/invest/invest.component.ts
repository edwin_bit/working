import { Utils } from '../../../shared/utils';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { InvestService, ModalService } from '../../../services/index.services';
import { ToastrService } from 'ngx-toastr';
import { Invest } from '../../../models/invest.model';

@Component({
	selector: 'app-invest',
	templateUrl: './invest.component.html',
	styleUrls: ['./invest.component.scss']
})
export class InvestComponent implements OnInit {

	public invest: Invest;
	public hidden = {
		dateTimeApply: true,
		fee: true,
		commission: true,
	};
	public brokers = Utils.BROKERS;
	public types = Utils.TYPES;

	constructor(
		public investService: InvestService,
		private tostr: ToastrService,
		private modalService: ModalService
	) {
		this.investService.selectedInvest$.subscribe(invest => {
			this.setEditInvest(invest);
		});
	}

	ngOnInit() {
		this.resetForm();
	}

	onSubmit(investForm: NgForm) {
		if (this.invest.total > 0) {
			if (this.invest.$key === null) {
				this.investService.insertInvest(this.invest);
			} else {
				this.investService.updateInvest(this.invest);
			}
			this.resetForm(investForm);
			this.modalService.close('invest-modal');
			this.tostr.success('Submitted Succcessfully', 'Invest Register');
		} else {
			this.tostr.error('Submitted Failed', 'Check all fields');
			return;
		}
	}

	resetForm(investForm?: NgForm) {
		this.invest = new Invest();
		this.fetchFields();
	}

	setEditInvest(invest: Invest) {
		this.invest = invest;
		this.fetchFields();
	}

	onChangedBroker() {
		this.fetchFields();
	}

	onChangedPriceBuy() {
		this.invest.priceBuy = this.validateNumber(this.invest.priceBuy, 'price buy');
		this.invest.total = Number((this.invest.quantity * this.invest.priceBuy).toFixed(2));
	}

	onChangedQuantity() {
		this.invest.quantity = this.validateNumber(this.invest.quantity, 'quantity');
		this.invest.total = Number((this.invest.quantity * this.invest.priceBuy).toFixed(2));
	}

	validateNumber(newValue: any, label?: string) {
		newValue = Number(String(newValue).replace(/[$&%#^,_-]/g, ''));

		// general validation for negative values
		if (newValue <= 0 || isNaN(newValue)) {
			this.tostr.warning(`Wrong ${label ? label : 'data'}`);
			return 0;
		} else {
			return newValue;
		}
	}

	onChangedPriceSell() {
		if (this.invest.total === 0) {
			return;
		}

		// TODO: bitso profit
		this.invest.priceSell = this.validateNumber(this.invest.priceSell, 'price sell');
		const spread = Number((this.invest.priceSell - this.invest.priceBuy).toFixed(8));
		const percentProfit = (spread / this.invest.priceBuy).toFixed(8);
		const earn = (this.invest.total * Number(percentProfit)).toFixed(2);

		this.invest.profit = Number(earn);
	}

	fetchFields() {
		if (Utils.shouldActivateApplyDate(this.invest.broker)) { // Scotiabank
			this.hidden.dateTimeApply = false;
			this.hidden.fee = true;
			this.hidden.commission = true;
			this.invest.dateTimeApply =
				Utils.plusDays(this.invest.dateTimeBuy, 2);
			this.invest.dateTimeSell	= this.invest.dateTimeApply;
		} else {
			this.hidden.dateTimeApply = true;
			this.hidden.fee = false;
			this.hidden.commission = false;
			this.invest.dateTimeApply = this.invest.dateTimeBuy;
			this.invest.dateTimeSell	= this.invest.dateTimeApply;
		}
	}

	onChangedInstrument() {
		this.invest.instrument = this.invest.instrument.trim().toUpperCase();
	}

	closeModal(id: string) {
		this.modalService.close(id);
	}
}
