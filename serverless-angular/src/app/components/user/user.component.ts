import { Component, OnInit } from '@angular/core';
import { UserService, AuthService } from '../../services/index.services';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../models/index.models';
import { Router } from '@angular/router';

@Component({
	selector: 'app-user',
	templateUrl: 'user.component.html',
	styleUrls: ['user.component.scss']
})
export class UserComponent implements OnInit {

	user: User = new User();
	profileForm: FormGroup;

	constructor(
		public userService: UserService,
		public authService: AuthService,
		private route: ActivatedRoute,
		private location: Location,
		private fb: FormBuilder,
    	private router: Router,
	) {

	}

	ngOnInit(): void {
		this.route.data.subscribe(routeData => {
			const data = routeData['data'];
			if (data) {
				this.user = data;
				this.createForm(this.user.firstName);
			}
		});
	}

	createForm(name) {
		this.profileForm = this.fb.group({
			name: [name, Validators.required]
		});
	}

	save(value) {
		this.userService.updateCurrentUser(value)
			.then(res => {
				console.log(res);
			}, err => console.log(err));
	}
}