import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { UserService } from '../../services/index.services';
import { User } from '../../models/index.models';

@Injectable()
export class UserResolver implements Resolve<User> {

	constructor(public userService: UserService, private router: Router) { }

	resolve(route: ActivatedRouteSnapshot): Promise<User> {

		const user = new User();

		return new Promise((resolve, reject) => {
			this.userService.getCurrentUser()
				.then(res => {
					if (res.providerData[0].providerId === 'password') {
						user.image = 'http://dsi-vd.github.io/patternlab-vd/images/fpo_avatar.png';
						user.firstName = res.displayName;
						user.provider = res.providerData[0].providerId;
						return resolve(user);
					} else {
						user.image = res.photoURL;
						user.firstName = res.displayName;
						user.provider = res.providerData[0].providerId;
						return resolve(user);
					}
				}, err => {
					this.router.navigate(['/login']);
					return reject(err);
				});
		});
	}
}