export { InvestsComponent } from './invests/invests.component';
export { InvestComponent } from './invests/invest/invest.component';
export { InvestListComponent } from './invests/invest-list/invest-list.component';
export { ModalComponent } from './modal/modal.component';