import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from "@angular/common";
import { Router } from "@angular/router";
import { Subscription } from 'rxjs';
import * as firebase from 'firebase/app';
import { AuthService } from '../../services/auth.service'
import { INVEST_ROUTES } from '../../shared/index.shared'

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent implements OnInit, OnDestroy {
  route: string;
  public INVEST_ROUTES = INVEST_ROUTES;
  currentUser: firebase.User;
  currentUserSubscription: Subscription;

  constructor(
    location: Location, router: Router,
    private auth: AuthService,
  ) {
    // User subscription
    this.currentUserSubscription = this.auth.currentUser.subscribe(user => {
      this.currentUser = user;
    });

    // Route subscription
    router.events.subscribe(val => {
      if (location.path() != "") {
        const endUrl = location.path().split('/');
        this.route = endUrl[1];
      }
    });
  }

  ngOnInit() { }
  
	logout() {
		this.auth.logout();
  }
  
  ngOnDestroy() {
    this.currentUserSubscription.unsubscribe();
  }
}