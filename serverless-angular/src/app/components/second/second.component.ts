import { Component, OnInit } from '@angular/core';
import { EchoService } from '../../services/index.services';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.scss']
})
export class SecondComponent implements OnInit {
  public response: Observable<any>;

  constructor(private echoService: EchoService) {}

  public ngOnInit(): void {
      this.response = this.echoService.makeCall();
  }
}