import { Component } from '@angular/core';
import { AuthService } from '../../services/index.services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent {

	public loginForm: FormGroup;
	public errorMessage = '';

	constructor(
		public authService: AuthService,
		private fb: FormBuilder
	) {
		this.createForm();
	}

	createForm() {
		this.loginForm = this.fb.group({
			email: ['', Validators.required],
			password: ['', Validators.required]
		});
	}

	tryLogin(value) {
		this.authService.login(value.email, value.password);
	}
}