import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  // styleUrls: ['./first.component.scss']
})
export class FirstComponent {
  
  users: any[];
   
  constructor(private activatedRoute: ActivatedRoute) { } 
  
  ngOnInit() {
    this.activatedRoute.data.subscribe((data: { users: any }) => {
      this.users = data.users; 
    }); 
  }
}

