import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FirstComponent } from './components/first/first.component';
import { SecondComponent } from './components/second/second.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UserComponent } from './components/user/user.component';
import { UserResolver } from "./components/user/ user.resolver";
import { AuthGuard } from './services/auth.guard';
import { InvestsComponent } from './components/index.components';
import { UserResolverService } from './services/index.services';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'history', component: InvestsComponent, canActivate: [AuthGuard] },
      { path: 'secondComponent', component: SecondComponent, canActivate: [AuthGuard] },
      { path: 'user', component: UserComponent, canActivate: [AuthGuard], resolve: { data: UserResolver } },
      { path: 'firstComponent', component: FirstComponent, canActivate: [AuthGuard], resolve: { users: UserResolverService } },
      { path: '**', redirectTo: 'login' },
    ], { useHash: false })
  ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }

